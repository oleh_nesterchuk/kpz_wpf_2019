﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class BallViewModel : ViewModelBase
    {
        #region private fields
        private bool _godMode;
        private int _x, _y;
        private int _radius, _speed;
        #endregion

        #region Properties
        public bool GodMode
        {
            get => _godMode; 
            set
            {
                _godMode = value;
                OnPropertyChanged(nameof(GodMode));
            }
        }
        public int X
        {
            get => _x;
            set
            {
                _x = value;
                OnPropertyChanged(nameof(X));
            }
        }
        public int Y
        {
            get => _y;
            set
            {
                _y = value;
                OnPropertyChanged(nameof(Y));
            }
        }
        public int Radius
        {
            get => _radius;
            set
            {
                _radius = value;
                OnPropertyChanged(nameof(Radius));
            }
        }
        public int Speed 
        { 
            get => _speed;
            set {
                _speed = value;
                OnPropertyChanged(nameof(Speed));
            } 
        }
        #endregion

    }
}
