﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace ViewModel
{
    public class Block : ViewModelBase
    {
        private int _x, _y;
        public Color Color { get; set; }
        public int Height { get; set; } = 100;
        public int Width { get; set; } = 400;
        public int X { get => _x; set
            {
                _x = value > 0 ? value : 0;
                OnPropertyChanged(nameof(X));
            }
        }
        public int Y { get => _y; set => _y = value > 0 ? value : 0; }
        public int HitsToDestroy { get; set; } = 1;
        public bool IsAlive { get; private set; } = true;
    }
}
