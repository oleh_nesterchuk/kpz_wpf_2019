﻿using AutoMapper;
using Model;
using Ninject;
using System.Windows;
using View.NInject;
using View.ViewModels;

namespace lab6_ui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //private Model.Records _recordModel;
        private RecordsViewModel _recordsViewModel;
        private IMapper _mapper;
        public App()
        {
            IKernel kernel = new StandardKernel(new AutoMapperModule());
            _mapper = kernel.Get<IMapper>();

            RecordsModel records = RecordsModel.Load();
            _recordsViewModel = _mapper.Map(records, new RecordsViewModel());

            var window = new MainWindow(_mapper) { DataContext = _recordsViewModel };
            window.Show();
        }
    }
}
