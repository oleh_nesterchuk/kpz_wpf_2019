﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using View.ViewModels;

namespace View
{
    /// <summary>
    /// Interaction logic for TableControl.xaml
    /// </summary>
    public partial class TableControl : UserControl
    {
        public TableControl()
        {
            RecordsList = new ObservableCollection<RecordViewModel>(
                ((RecordsViewModel)Application.Current.MainWindow.DataContext).RecordsList);
            InitializeComponent();
        }

        public ObservableCollection<RecordViewModel> RecordsList { get; set; }

        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register(nameof(Header), typeof(string), typeof(TableControl), new PropertyMetadata("default text"));
    }
}
