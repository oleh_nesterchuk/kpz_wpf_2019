﻿using Model;

namespace View.ViewModels
{
    public class BallViewModel : BaseViewModel
    {
        #region private fields
        private int _x, _y, _radius;
        private Point _direction;
        private bool _isOut;
        #endregion

        #region Properties
        public int X
        {
            get => _x;
            set
            {
                _x = value;
                OnPropertyChanged(nameof(X));
            }
        }
        public int Y
        {
            get => _y;
            set
            {
                _y = value;
                OnPropertyChanged(nameof(Y));
            }
        }
        public int Radius
        {
            get => _radius;
            set
            {
                _radius = value;
                OnPropertyChanged(nameof(Radius));
            }
        }
        public Point Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                OnPropertyChanged(nameof(Direction));
            }
        }
        public bool IsOut
        {
            get => _isOut;
            set
            {
                _isOut = value;
                OnPropertyChanged(nameof(IsOut));
            }
        }
        #endregion
    }
}
