﻿using System.Collections.ObjectModel;

namespace View.ViewModels
{
    public class RecordsViewModel : BaseViewModel
    {
        public ObservableCollection<RecordViewModel> RecordsList { get; set; }
        public RecordsViewModel() { }
    }
}
