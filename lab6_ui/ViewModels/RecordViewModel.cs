﻿namespace View.ViewModels
{
    public class RecordViewModel : BaseViewModel
    {
        private int _mapLevel, _score;
        private string _description;

        public int MapLevel
        {
            get => _mapLevel;
            set
            {
                _mapLevel = value;
                OnPropertyChanged(nameof(MapLevel));
            }
        }
        public int Score
        {
            get => _score;
            set
            {
                _score = value;
                OnPropertyChanged(nameof(Score));
            }
        }
        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }
    }
}
