﻿using Model;

namespace View.ViewModels
{
    public class BlockViewModel : BaseViewModel
    {
        #region private fields
        private Colors _color;
        private int _height, _width, _x, _y, _hitsToDestroy;
        private bool _isAlive;
        #endregion

        #region Properties
        public Colors Color 
        { 
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                OnPropertyChanged(nameof(Color));
            }
        }
        public int Height 
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
                OnPropertyChanged(nameof(Height));
            }
        }
        public int Width
        {
            get
            {
                return _width;
            }
            set
            {
                _width = value;
                OnPropertyChanged(nameof(Width));
            }
        }
        public int X
        {
            get { return _x; } 
            set
            {
                _x = value;
                OnPropertyChanged(nameof(X));
            }
        }
        public int Y 
        {
            get { return _y; } 
            set
            {
                _y = value;
                OnPropertyChanged(nameof(Y));
            }
        }
        public int HitsToDestroy
        {
            get
            { return _hitsToDestroy; }
            set
            {
                _hitsToDestroy = value;
                OnPropertyChanged(nameof(HitsToDestroy));
            }
        }
        public bool IsAlive
        {
            get { return _isAlive; }
            set { _isAlive = value; OnPropertyChanged(nameof(IsAlive)); }
        }
        #endregion
    }
}
