﻿using AutoMapper;
using Model;
using System.Windows.Input;
using View.Commands;

namespace View.ViewModels
{
    public class PlatformViewModel : BaseViewModel
    {
        private IMapper _mapper;
        private int _x, _y, _width, _height;
        private ICommand _moveCommand;
        public int X
        {
            get => _x;
            set
            {
                _x = value;
                OnPropertyChanged(nameof(X));
            }
        }
        public int Y
        {
            get => _y;
            set
            {
                _y = value;
                OnPropertyChanged(nameof(Y));
            }
        }
        public int Width
        {
            get => _width;
            set
            {
                _width = value;
                OnPropertyChanged(nameof(Width));
            }
        }
        public int Height
        {
            get => _height;
            set
            {
                _height = value;
                OnPropertyChanged(nameof(Height));
            }
        }
        public ICommand MoveCommand
        {
            get => _moveCommand ??= new MovePlatformCommand(this);
        }

        public PlatformViewModel(IMapper mapper)
        {
            _mapper = mapper;
        }

        public void ChangeLength(int delta)
        {

        }

        public void Move(Point vector)
        {
            var model = _mapper.Map(this, new PlatformModel(Width));
            model.Move(vector);
            X = model.X;
        }
    }
}
