﻿using AutoMapper;
using Model;
using System.Collections.ObjectModel;
using System.Timers;

namespace View.ViewModels
{
    public class GameViewModel : BaseViewModel
    {
        private readonly IMapper _mapper;
        private int _lives, _points;
        private double _updateFrequency;

        #region Properties
        public BallViewModel Ball { get; set; }
        public MapViewModel Map { get; set; }
        public PlatformViewModel Platform { get; set; }
        public ObservableCollection<BaseViewModel> Items { get; set; }
        public int Lives
        {
            get => _lives;
            set
            {
                _lives = value;
                OnPropertyChanged(nameof(Lives));
            }
        }
        public int Points
        {
            get => _points;
            set
            {
                _points = value;
                OnPropertyChanged(nameof(Points));
            }
        }
        public double UpdateFrequency
        {
            get => _updateFrequency;
            set
            {
                _updateFrequency = value;
                OnPropertyChanged(nameof(UpdateFrequency));
            }
        }
        #endregion

        public GameViewModel(IMapper mapper)
        {
            _mapper = mapper;
            Lives = 5;
            UpdateFrequency = 0.01;
            Timer timer = new Timer(_updateFrequency);
            timer.Elapsed += Tick;
            timer.Enabled = true;
        }

        private void Tick(object source, ElapsedEventArgs e)
        {
            var gameModel = _mapper.Map(this, new GameModel());
            gameModel.Tick();
            Ball.X = gameModel.Ball.X; Ball.Y = gameModel.Ball.Y;
            for (int i = 0; i < Map.Blocks.Count; ++i)
            {
                Map.Blocks[i].IsAlive = gameModel.Map.Blocks[i].IsAlive;
            }
            Items = new ObservableCollection<BaseViewModel>(Map.Blocks);
            Items.Add(Ball);
            Items.Add(Platform);
        }
    }
}
