﻿using AutoMapper;
using Model;
using System.Collections.ObjectModel;

namespace View.ViewModels
{
    public class MapViewModel : BaseViewModel
    {
        private IMapper _mapper;
        private int _width, _height;
        public int Width
        {
            get => _width;
            set
            {
                _width = value;
                OnPropertyChanged(nameof(Width));
            }
        }
        public int Height
        {
            get => _height;
            set
            {
                _height = value;
                OnPropertyChanged(nameof(Height));
            }
        }
        public string MapName { get; set; }
        public ObservableCollection<BlockViewModel> Blocks { get; set; }

        public MapViewModel()
        {
        }
    }
}
