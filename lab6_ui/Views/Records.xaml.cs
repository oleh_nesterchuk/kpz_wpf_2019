﻿using AutoMapper;
using Model;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using View.ViewModels;

namespace lab6_ui
{
    /// <summary>
    /// Interaction logic for Records.xaml
    /// </summary>
    public partial class Records : Window
    {
        private IMapper _mapper;
        public Records(IMapper mapper)
        {
            InitializeComponent();

            _mapper = mapper;
            Application.Current.MainWindow.Hide();
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Window_Closed(sender, new EventArgs());
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            var recordModel = _mapper.Map((RecordsViewModel)DataContext, new RecordsModel());
            recordModel.Save();
            DoubleAnimation animation = new DoubleAnimation()
            {
                From = 1.0,
                To = 0.0,
                Duration = new Duration(TimeSpan.FromSeconds(1))
            };
            animation.Completed += (sender, args) => this.Close();
            this.BeginAnimation(Window.OpacityProperty, animation);
            Application.Current.MainWindow.Show();
        }

    }
}
