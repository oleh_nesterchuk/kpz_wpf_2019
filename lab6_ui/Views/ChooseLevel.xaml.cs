﻿using AutoMapper;
using System.Windows;
using System.Windows.Controls;

namespace lab6_ui
{
    /// <summary>
    /// Interaction logic for ChooseLevel.xaml
    /// </summary>
    public partial class ChooseLevel : Window
    {
        private IMapper _mapper;
        public ChooseLevel(IMapper mapper)
        {
            InitializeComponent();
            _mapper = mapper;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // level
            string content = (sender as Button).Content.ToString();
            int levelNum = int.Parse(content.Substring(5));
            new GameWindow(levelNum, _mapper).Show();
            this.Close();
        }
    }
}
