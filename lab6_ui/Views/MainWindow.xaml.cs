﻿using AutoMapper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace lab6_ui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Dictionary<string, Action> _menus = new Dictionary<string, Action>();
        private MediaPlayer mediaPlayer = new MediaPlayer();
        private IMapper _mapper;

        public MainWindow(IMapper mapper)
        {
            _mapper = mapper;
            _menus.Add("New Game", () => new ChooseLevel(_mapper).Show());
            _menus.Add("Records", () => new Records(_mapper) { DataContext = DataContext }.Show());
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            try
            {
                _menus[b.Content.ToString()].Invoke();
            }
            catch (KeyNotFoundException) { MessageBox.Show("Menu item hasn't been added yet"); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mi = sender as MenuItem;

            MainMenuGrid.Background = new ImageBrush()
            {
                ImageSource = new BitmapImage(new Uri("pack://application:,,,/Images/"
                    + mi.Header + ".jpg")),
                Stretch = Stretch.UniformToFill
            };
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.MediaFailed += (o, args) =>
             {
                 MessageBox.Show("Media Failed!! " + args.ErrorException);
                 MessageBox.Show(AppDomain.CurrentDomain.BaseDirectory);
             };
            mediaPlayer.Open(new Uri("../../../soundtrack.mp3", UriKind.RelativeOrAbsolute));
            mediaPlayer.Play();
        }

        private void AddRecord()
        {
            var records = Model.RecordsModel.Load();
            var mapLevel = records.RecordsList.Select(x => x.MapLevel).Max();
            records.RecordsList.Add(new RecordModel()
            {
                MapLevel = mapLevel + 1,
                Score = mapLevel * 1050,
                Description = "Ez map #" + (mapLevel + 1)
            });

            records.Save();
        }

        private void ToggleMusic(object sender, RoutedEventArgs e)
        {
            mediaPlayer.Pause();
        }
    }
}
