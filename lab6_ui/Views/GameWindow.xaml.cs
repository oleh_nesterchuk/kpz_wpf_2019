﻿using AutoMapper;
using Model;
using System.Collections.ObjectModel;
using System.Windows;
using View.ViewModels;

namespace lab6_ui
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private IMapper _mapper;
        public GameViewModel Game { get; set; }
        public ObservableCollection<BaseViewModel> Items { get; set; }
        public GameWindow(int level, IMapper mapper)
        {
            _mapper = mapper;
            Game = new GameViewModel(_mapper)
            {
                Platform = new PlatformViewModel(_mapper)
                {
                    Width = 80,
                    Height = 20,
                    X = 500,
                    Y = 650
                },
                Ball = new BallViewModel()
                {
                    X = 500,
                    Y = 350,
                    Radius = 20,
                    Direction = new Model.Point() { X = 0, Y = 2 }
                },
                Map = _mapper.Map(new MapModel(), new MapViewModel())
            };
            Items = new ObservableCollection<BaseViewModel>(Game.Map.Blocks);
            Items.Add(Game.Ball);
            Items.Add(Game.Platform);

            InitializeComponent();
        }
    }
}
