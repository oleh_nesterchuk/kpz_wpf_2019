﻿using System;
using System.Windows.Input;
using View.ViewModels;

namespace View.Commands
{
    public class MovePlatformCommand : ICommand
    {
        private PlatformViewModel _platform;
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            int delta = int.Parse(parameter.ToString());
            _platform.Move(new Model.Point() { X = delta });
        }

        public MovePlatformCommand(PlatformViewModel platform)
        {
            _platform = platform;
        }
    }
}
