﻿using Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using WinMedia = System.Windows.Media;
using System.Windows.Media.Imaging;

namespace View.ValueConverters
{
    public class ColorConverter : IValueConverter
    {
        static Dictionary<Colors, WinMedia.Color> colors = new Dictionary<Colors, WinMedia.Color>()
        {
            [Colors.Red] = WinMedia.Colors.Red,
            [Colors.Green] = WinMedia.Colors.Green,
            [Colors.Blue] = WinMedia.Colors.Blue,
        };
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new WinMedia.SolidColorBrush(colors[(Colors)value]);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
