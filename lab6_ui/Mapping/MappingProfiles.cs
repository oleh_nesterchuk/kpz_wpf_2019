﻿using AutoMapper;
using Model;
using View.ViewModels;

namespace View.Mapping
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<BallModel, BallViewModel>().ReverseMap();

            CreateMap<BlockModel, BlockViewModel>().ReverseMap();

            CreateMap<GameModel, GameViewModel>().ReverseMap();

            CreateMap<MapModel, MapViewModel>().ReverseMap();

            CreateMap<RecordModel, RecordViewModel>().ReverseMap();

            CreateMap<RecordsModel, RecordsViewModel>().ReverseMap();

            CreateMap<PlatformModel, PlatformViewModel>().ReverseMap();
        }
    }
}
