﻿namespace Model
{
    public class BallModel
    {
        public int X { get; set; } = MapSettings.Width / 2;
        public int Y { get; set; } = MapSettings.Height / 2;
        public int Radius { get; set; } = 10;
        public Point Direction { get; set; }
        public bool IsOut { get; set; }

        public BallModel() => X = Y = 0;

        public void Move()
        {
            X += Direction.X;
            Y += Direction.Y;
            ValidatePosition();
        }

        private void ValidatePosition()
        {
            if (X + Radius > MapSettings.Width)
            {
                Direction.X = -Direction.X;
                X = MapSettings.Width - Radius;
            }
            else if (X <= 0) 
            {
                Direction.X = -Direction.X;
                X = 0;
            }
            if (Y + Radius > MapSettings.Height)
            {
                IsOut = true;
            }
            else if (Y < 0)
            {
                Y = 0;
                Direction.Y = -Direction.Y;
            }
        }
    }
}
