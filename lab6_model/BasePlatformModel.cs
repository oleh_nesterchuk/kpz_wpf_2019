﻿using System;

namespace Model
{
    public class PlatformModel
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public PlatformModel()
        {
            X = 0; Y = 0;
        }
        public PlatformModel(int length) : this()
        {
            Width = length;
        }

        public void ChangeLength(int delta)
        {
            Width += Math.Abs(delta) + Width < 200 || Math.Abs(delta) + Width > 5 ? delta : 0;
        }

        public void Move(Point vector)
        {
            if (X + vector.X < 0)
            {
                X = 0;
            }
            else if (X + Width + vector.X > MapSettings.Width)
                X = MapSettings.Width - Width;
            else X += vector.X;
        }
    }
}
