﻿namespace Model
{
    public class RecordModel
    {
        public int MapLevel { get; set; }
        public int Score { get; set; }
        public string Description { get; set; }
        public RecordModel() { }
    }
}
