﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    // Реалізація перелічуваного типу
    [Flags]
    public enum Colors
    {
        Red = 1, Green = 2, Blue = 4
    }
}
