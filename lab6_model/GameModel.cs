﻿namespace Model
{
    /// <summary>
    /// Contains game flow logic, container for basic game objects
    /// </summary>
    public class GameModel
    {
        public BallModel Ball { get; set; }
        public MapModel Map { get; set; }
        public PlatformModel Platform { get; set; }
        public int Lives { get; set; }
        public int Points { get; set; }
        public double UpdateFrequency { get; set; }

        public GameModel()
        {
        }

        /// <summary>
        /// returns false if game is over
        /// </summary>
        public bool Tick()
        {
            Ball.Move();
            if (Ball.IsOut)
            {
                if (--Lives < 0)
                {
                    return false;
                }
            }

            CheckPlatformInteraction();
            CheckBlocksInteraction();

            return true;
        }

        private void CheckPlatformInteraction()
        {
            if (AreOverlapped(Ball.X, Ball.Y, Ball.X + Ball.Radius, Ball.Y + Ball.Radius,
                    Platform.X, Platform.Y, Platform.X + Platform.Width, Platform.Y + Platform.Height))
            { 
                int hitPosition = (Platform.X + Platform.Width) - (Ball.X + Ball.Radius);
                int platformCenter = Platform.Width / 2;
                if (hitPosition > platformCenter)
                {
                    Ball.Direction.X--;
                }
                else if (hitPosition < platformCenter)
                {
                    Ball.Direction.X++;
                }
                Ball.Direction.Y *= -1;
            }
        }

        private void CheckBlocksInteraction()
        {
            foreach (BlockModel b in Map.Blocks)
            {
                if (AreOverlapped(Ball.X, Ball.Y, Ball.X + Ball.Radius, Ball.Y + Ball.Radius,
                     b.X, b.Y, b.X + b.Width, b.Y + b.Height) && b.IsAlive)
                {
                    b.Hit();
                    if (Ball.X == b.X || Ball.X == b.X + b.Width)
                    {
                        Ball.Direction.X *= -1;
                    }
                    if (Ball.Y == b.Y || Ball.Y == b.Y + b.Height)
                    {
                        Ball.Direction.Y *= -1;
                    }
                } 
            }
        }

        private bool AreOverlapped(int aX1, int aY1, int aX2, int aY2, int bX1, int bY1, int bX2, int bY2)
        {
            if ((aX1 <= bX2 && aX2 >= bX1 && aY1 <= bY2 && aY2 >= bY1))
            {
                return true;
            }
            return false;
        }
    }
}
