﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public static class MapSettings
    {
        public static int Width { get; set; } = 1000;
        public static int Height { get; set; } = 700;
    }
}
