﻿using System;

namespace Model
{
    public class BlockModel
    {
        private int _x, _y;
        public Colors Color { get; set; }
        public int Height { get; set; } = 100;
        public int Width { get; set; } = 400;
        public int X { get => _x; set => _x = value > 0 ? value : 0; }
        public int Y { get => _y; set => _y = value > 0 ? value : 0; }
        public int HitsToDestroy { get; set; } = 1;
        public bool IsAlive { get; private set; } = true;

        public static bool IsVisible { get; set; }

        public BlockModel() : this(500, 500, Colors.Red) { }

        public BlockModel(int x, int y, Colors color)
        {
            Color = color; X = x; Y = y;
        }

        public BlockModel(int x, int y, Colors color, int width, int height) : this(x, y, color)
        {
            Height = height; Width = width;
        }

        public void Hit()
        {
            if (--HitsToDestroy <= 0)
            {
                IsAlive = false;
            }
        }
    }
}
