﻿using System;
using System.Collections.Generic;

namespace Model
{
    public class MapModel
    {
        public int Width { get; set; } = 1000;
        public int Height { get; set; } = 700;
        public string MapName { get; set; }
        public List<BlockModel> Blocks { get; set; } = new List<BlockModel>();
        private MapGenerator _generator;
        
        public MapModel()
        {
            _generator = new MapGenerator(this);
        }

        private class MapGenerator
        {
            private Random r = new Random((int)DateTime.Now.Ticks);
            private readonly MapModel _map;
            public MapGenerator(MapModel map)
            {
                _map = map;
                Generate();
            }
            private void Generate()
            {
                var colors = new List<int>() { 1, 2, 4 };

                int blockCount = r.Next(1, 20);
                int blockWidth = r.Next(50, 75);
                int blockHeight = r.Next(20, 40);

                for (int j = 0; j < blockCount; j++)
                {
                    _map.Blocks.Add(new BlockModel()
                    {
                        Height = blockHeight,
                        Width = blockWidth,
                        X = r.Next(0, _map.Width),
                        Y = r.Next(0, _map.Height - 300),
                        Color = (Colors)colors[r.Next(0, 3)]
                    }) ;
                }
            }
        }
    }
}
