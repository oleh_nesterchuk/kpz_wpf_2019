﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace Model
{
    [DataContract]
    public class RecordsModel
    {
        [DataMember]
        public List<RecordModel> RecordsList { get; set; }

        public RecordsModel()
        {
            RecordsList = new List<RecordModel>() {
                new RecordModel()
                    { Score = 120, MapLevel = 1, Description = "EzPz" }
            };
        }

        public static string Path { get; set; } = "records.dat";
        public static RecordsModel Load()
        {
            if (File.Exists(Path))
            {
                var formatter = new DataContractSerializer(typeof(RecordsModel));
                using (var stream = new FileStream(Path, FileMode.Open))
                {
                    return (RecordsModel)formatter.ReadObject(stream);
                }
            }
            return new RecordsModel();
        }

        public void Save()
        {
            var formatter = new DataContractSerializer(typeof(RecordsModel));
            using (var stream = new FileStream(Path, FileMode.Create))
            {
                formatter.WriteObject(stream, this);
            }
        }
    }
}
